import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Ingredient} from "./Ingredient";


@Entity()
export class UnitQuantity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    gramEqual: number;

    @OneToMany(type => Ingredient, ingredient => ingredient.unitQuantity)
    ingredients: Ingredient[]

}