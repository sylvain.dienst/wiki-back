import {Column, Entity, ManyToMany, PrimaryGeneratedColumn} from "typeorm";


@Entity()
export class Step {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    order: number;

    @Column()
    content: string
    
}