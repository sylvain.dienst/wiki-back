import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {UnitQuantity} from "./UnitQuantity";


@Entity()
export class Ingredient{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(type => UnitQuantity, unitQuantity => unitQuantity.ingredients)
    unitQuantity: UnitQuantity
}