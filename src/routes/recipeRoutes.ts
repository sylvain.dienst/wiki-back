import {Router} from "express";
import {Recipe} from "../entity/recipe/Recipe";
import {create, deleteRecipe, find, findAll, update} from "../controler/recipeController";

export default () => Router()
    .get('/', findAll)
    .get('/:id', find)
    .post('/', create)
    .put('/:id', update)
    .delete('/:id', deleteRecipe)