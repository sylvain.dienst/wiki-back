import {Router} from 'express'
import {changePassword, login} from '../controler/authController'

export default () => Router()
    .post('/login', login)
    .post('/changePassword', changePassword)