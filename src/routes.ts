import {Application} from 'express';
import createAuthRouter from "./routes/authRoutes";
import createRecipeRouter from "./routes/recipeRoutes"

export default (app : Application) => {
    app.use('/auth', createAuthRouter());
    app.use('/recipe', createRecipeRouter());
}