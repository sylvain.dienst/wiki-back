import {UnauthorizedError} from "../exception/UnauthorizedError";
import {ConnectionError} from "../exception/ConnectionError";
import {NotFoundError} from "../exception/NotFoundError";


export const handleError = (e: Error) => {
    if (e instanceof UnauthorizedError || e instanceof ConnectionError || e instanceof NotFoundError) {
        return {status: e.getStatus(), message: e.message}
    } else {
        console.log(e)
        return {status: 500, message: "Server Error"}
    }
};